FROM       ruby:2.5

# Depending on the docker image you're using, you may need to specify a
# different workdir, or even not set one and use the perscribed one from the
# base image.


RUN        mkdir -p /src

WORKDIR    /src

ADD        . /src

# Do any other initializations steps here.

ENTRYPOINT [ "/src/bin/entrypoint.sh" ]
CMD        [ "pass" ]
