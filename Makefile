DOCKER               ?= docker
DOCKER_COMPOSE       ?= docker-compose
APPLICATION_SERVICES  = cli
RESOURCE_SERVICES     =
COMPOSE_SHELL         = ${DOCKER_COMPOSE} run --rm shell

default_target: initialize

initialize:
	$(MAKE) images
	$(MAKE) start
	$(MAKE) setup
	@echo "Everything initialized and started."

images: pull
	@echo "Building base && base_development"
	@${DOCKER_COMPOSE} build base
	@${DOCKER_COMPOSE} build base_development
	@echo "Images are prepped. Run 'make start' to set things up"

pull:
	@echo "Pulling all images"
	# pull --ignore-pull-failures has been broken since compose 1.13
	# https://github.com/docker/compose/issues/4377
	# @${DOCKER_COMPOSE} pull --ignore-pull-failures
	#
	# For now, we have to manage this explicitely...
	@${DOCKER_COMPOSE} pull base_external ${RESOURCE_SERVICES}

setup:
	@${COMPOSE_SHELL} setup

run: start-resources
	@${DOCKER_COMPOSE} up ${APPLICATION_SERVICES}

start:
	@${DOCKER_COMPOSE} up -d ${RESOURCE_SERVICES} ${APPLICATION_SERVICES}

# Send a sigterm before compose stop to get them just to shut down.
# Remove that if you want more graceful shutdowns of the application.
stop:
	@${DOCKER_COMPOSE} kill -s SIGTERM ${APPLICATION_SERVICES}
	@${DOCKER_COMPOSE} stop ${APPLICATION_SERVICES}

start-resources:
	@if [ ! -z ${RESOURCE_SERVICES} ] ; then ${DOCKER_COMPOSE} up -d ${RESOURCE_SERVICES}; fi

stop-resources:
	@${DOCKER_COMPOSE} stop

shell:
	@${COMPOSE_SHELL}

test:
	 @${COMPOSE_SHELL} "test"

implode:
	@${DOCKER_COMPOSE} down --volumes --rmi all

# Delete the application containers
rm-application:
	@${DOCKER_COMPOSE} rm -f ${APPLICATION_SERVICES}

production:
	@${DOCKER_COMPOSE} build base
	@echo "Your image is ready and tagged as planner_base:latest"

reset:
	@echo "Fully resetting the environment"
	$(MAKE) implode
	$(MAKE) initialize

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' | xargs