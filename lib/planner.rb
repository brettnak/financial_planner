module Planner
  def self.require(glob)
    flist = Dir.glob(glob)
    flist.each { |f| Kernel.require f }
  end

  require './models/**/*.rb'
end

require 'time'
