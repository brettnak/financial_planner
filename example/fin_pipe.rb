require 'planner'

mortgage = Planner::Debt::Mortgage.new(
  :name => "Home Loan"
  :sale_value => 300_000,
  :origination => 200_000,
  :apr => 3.625,
  :appreciation_rate => 9,
)

person_1_income = Planner::Income.new(
  name: "Job 1",
  yearly: 100_000
  on: [1, 15],
  start: :auto,
  stop:
)

person_2_income = Planner::Income.new(
  name: "Job 2",
  yearly: 60_000,
  on: [31],
)

savings = Planner::Account.new(
  name: "Main Savings Account",
  starting_balance: 80_000.00,
  interest_rate: 0.14,
  # Think of these as virtual accounts.
  # Once these goals are filled, no more money will go into
  # savings.
  goals: {
    car_down_payment: 20_000,
    emergency_fund: 60_000,
    vacation_fund: 7_000,
  }
)

job_401k = Planner::Job401k.new(
  name: "Job Provided 401k",
  starting_balance: 15_000
)

investment = Planner::InvestmentAccount(
  name: "My Favorite Investment Robot",
  apy: 8, # Pre tax interest rate, average yearly return
)

# This is only for _paying_ for things.  It should not include
# any planned or scheduled transactions, such as loan payments
# or setting asside money for investments.
expenses = Planner::Expenses.new(
  # These will be applied at the beginning of each month, as that's
  # the least advantageous way to account for expenses.
  period: :monthly,
  name: "Budgeting",
  buckets: {
    stuff: 1_500,
    food: 40.0,
    gas: 200.0,
    subscriptions: {
      github: 7,
      drobpox: 10,
      netflix: 10,
      youtube_tv: 30,
      youtube_premium: 10,
    }
  }
)

Planner::Plan do |plan|
  plan.start_time = Time.parse("October 1, 2018 00:00:01Z")

  plan.people << { name: 'person_1', birthday: Date.iso8601("1985/07/03"), income: person_1_income }
  plan.people << { name: 'person_2', birthday: Date.iso8601("1983/04/13"), income: person_2_income }

  plan.expenses << expenses

  plan.on( each: 4 )

  plan.recurring do |transactions|
    transactions << Planner::Transaction(savings, income, 3_000)
    transactions << Planner::Transaction
  end
end
