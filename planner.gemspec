lib_path  = "./lib"
spec_path = "./spec"
bin_path  = "./bin"

libs  = Dir.glob("#{lib_path}/**/*.rb")
specs = Dir.glob("#{spec_path}/**/*.rb")
bins  = Dir.glob("#{bin_path}/**/*")

flist = libs + specs + bins
flist << "planner.gemspec"
flist << "README.md"

require_relative './lib/version'

summary = <<-EOD
A gem to help do financial planning.  Can model loans, income
and taxes.

Implementation pipeline based.
EOD

Gem::Specification.new do |gem|
  gem.name          = "planner"
  gem.version       = Planner::VERSION
  gem.authors       = ["Brett Nakashima"]
  gem.email         = ["brettnak@gmail.com"]
  gem.description   = "Makefile"
  gem.summary       = summary

  gem.homepage      = "http://gitlab.com/brettnak/financial_planner_gem"

  gem.files         = flist
  gem.executables   = bins.map{ |f| File.basename(f) }
  gem.test_files    = specs
  gem.require_paths = ["lib/"]

  gem.add_development_dependency 'rspec', '~> 3.1'
  gem.add_development_dependency 'trollop', '~> 2.1'

  gem.license = "MIT"
end
